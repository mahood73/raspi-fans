# Raspi-Fans

Control a case fan by means of an HTTP page.
I'm using this case
https://thepihut.com/products/8-slot-cloudlet-cluster-case?variant=37531942387907
and this fan 'shim'
https://thepihut.com/collections/raspberry-pi-fan-hats/products/auto-fan-control-module-for-raspberry-pi

## Overview

Provides a simple status page, showing current temperature, fan status (on/off) and a button for each state.
You can also GET http://host:8000/on to switch fan on, and GET http://host:8000/off to switch fan off

### Notes

_. Does not have any security - anyone with access to the network can switch your fans on and off
_. Web page doesn't autoupdate.

# Install

Clone the repository, and load the requirements.

`pip3 install -r requirements.txt`

Copy `sample.env` to `.env` and set the PIN according to your setup. Default is fine for the example above.

Test:
`python3 fan-server.py`
Visit http://hostname:8000 - should see temperature and fan status.

## Set up Systemd

### Install requirements to run as root

`sudo pip3 install -r requirements.txt`

### Create daemon file

Create file `/etc/systemd/system/fan.service`

```
[Unit]
Description=Control Case Fan via Web
Documentation=https://gitlab.com/mahood73/raspi-fans
After=network.target auditd.service

[Service]
ExecStart=/usr/bin/python3 /home/pi/raspi-fans/fan-server.py
WorkingDirectory=/home/pi/raspi-fans
Restart=on-failure
Type=simple

[Install]
WantedBy=multi-user.target
```

### Enable and run

```
sudo systemctl daemon-reload
sudo systemctl enable fan
sudo systemctl start fan
```

Check website again.
