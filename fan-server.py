import RPi.GPIO as GPIO
import os
import socket
import json 
from http.server import BaseHTTPRequestHandler, HTTPServer
from dotenv import load_dotenv

fanPin = 4

my_name = socket.gethostname()
my_ip = socket.gethostbyname(my_name)
my_port = 8000

def get_cpu_temp():
    """
    Obtains the current value of the CPU temperature.
    :returns: Current value of the CPU temperature if successful, zero value otherwise.
    :rtype: float
    """
    # Initialize the result.
    result = 0.0
    # The first line in this file holds the CPU temperature as an integer times 1000.
    # Read the first line and remove the newline character at the end of the string.
    if os.path.isfile('/sys/class/thermal/thermal_zone0/temp'):
        with open('/sys/class/thermal/thermal_zone0/temp') as f:
            line = f.readline().strip()
        # Test if the string is an integer as expected.
        if line.isdigit():
            # Convert the string with the CPU temperature to a float in degrees Celsius.
            result = float(line) / 1000
    # Give the result back to the caller.
    return result

class MyServer(BaseHTTPRequestHandler):
    """ A special implementation of BaseHTTPRequestHander for reading data from
        and control GPIO of a Raspberry Pi
    """

    def do_HEAD(self):
        """ do_HEAD() can be tested use curl command
            'curl -I http://server-ip-address:port'
        """
        self.send_response(200)
        self.send_header('Content-type', 'application/json')
        self.end_headers()

    def do_GET(self):
        """ do_GET() can be tested using curl command
            'curl http://server-ip-address:port'
        """
        # Fetch CPU temperature
        temp = get_cpu_temp()
        
        # Set up GPIO pin
        GPIO.setmode(GPIO.BCM)
        GPIO.setwarnings(False)
        GPIO.setup(fanPin, GPIO.OUT)

        # Read current status
        state = GPIO.input(fanPin)
        status = ''

        # Set status text for JSON
        if state:
            status='on'
        else:
            status='off'

        # If we're asking to change status:
        if self.path=='/on':
            GPIO.output(fanPin, GPIO.HIGH)
            status='on'
        elif self.path=='/off':
            GPIO.output(fanPin, GPIO.LOW)
            status='off'

        # And return the JSON we need
        data = {"hostname": my_name, "temperature": temp, "fan": status}

        self.do_HEAD()
        self.wfile.write(json.dumps(data).encode('utf-8'))


if __name__ == '__main__':
    http_server = HTTPServer((my_ip, my_port), MyServer)
    print("Server Starts - http://%s:%s" % (my_ip, my_port))

    try:
        http_server.serve_forever()
    except KeyboardInterrupt:
        http_server.server_close()
